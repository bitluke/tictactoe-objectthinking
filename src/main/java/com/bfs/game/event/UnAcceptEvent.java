package com.bfs.game.event;

import com.bfs.game.Player;

/**
 * Created by lukman on 5/14/16.
 */
public class UnAcceptEvent {
    private Player player;

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
