package com.bfs.game.event;

import com.bfs.game.Location;
import com.bfs.game.Player;

/**
 * Created by lukman on 5/9/16.
 */
public class PlayEvent {

    private Location boardLocation;
    private Player played;

    public Location getBoardLocation() {
        return boardLocation;
    }

    public void setBoardLocation(Location boardLocation) {
        this.boardLocation = boardLocation;
    }

    public void setPlayed(Player played) {
        this.played = played;
    }

    public Player getPlayed() {
        return played;
    }
}
