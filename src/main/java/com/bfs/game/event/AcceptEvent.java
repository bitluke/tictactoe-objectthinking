package com.bfs.game.event;

import com.bfs.game.Player;

/**
 * Created by lukman on 5/14/16.
 */
public class AcceptEvent {
    private Player nextPlayer;

    public void setNextPlayer(Player nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    public Player getNextPlayer() {
        return nextPlayer;
    }
}
