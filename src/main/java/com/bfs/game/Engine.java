package com.bfs.game;

import com.bfs.game.event.AcceptEvent;
import com.bfs.game.event.PlayEvent;
import com.bfs.game.event.UnAcceptEvent;
import com.bfs.game.listener.AcceptListener;
import com.bfs.game.listener.PlayListener;
import com.bfs.game.listener.UnAcceptListener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the core of the application, that is it represents how the game is been handlePlayEvent.
 * Created by lukman on 4/10/16.
 */
public class Engine implements PlayListener {
    private List<AcceptListener> acceptListeners = new ArrayList<>(2);
    private List<UnAcceptListener> unAcceptListeners = new ArrayList<>(2);
    private Board board;
    private Player firstPlayer;
    private Player secondPlayer;
    private InputStream console = System.in;

    public Engine() {
        this(Board.getInstance(), new Player(), new Player());
    }

    public void initEngine() {

        firstPlayer.acceptName(console);
        firstPlayer.acceptSymbol(console);

        secondPlayer.acceptName(console);
        secondPlayer.acceptSymbol(console);

        firstPlayer.addPlayListener(this);
        secondPlayer.addPlayListener(this);

        addAcceptListeners(firstPlayer);
        addUnAcceptListeners(firstPlayer);

        addAcceptListeners(secondPlayer);
        addUnAcceptListeners(secondPlayer);

    }

    public Engine(Board board, Player firstPlayer, Player secondPlayer) {
        this.board = board;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
    }

    public void stop() {
        firstPlayer.removePlayListener(this);
        secondPlayer.removePlayListener(this);

        removeAcceptListeners(firstPlayer);
        removeAcceptListeners(secondPlayer);

        board.reset();

    }


    private void setLocation(Location location, Symbol symbol) {
        board.setLocation(location, symbol);
    }

    private boolean hasWon(Symbol symbol) {
        return board.hasWon(symbol);
    }

    private boolean boardFull() {
        return board.isFull();
    }

    @Override
    public void handlePlayEvent(PlayEvent playEvent) {
        Player whoPlayed = playEvent.getPlayed();

        int countBefore = filledSpaces();
        setLocation(playEvent.getBoardLocation(), whoPlayed.getSymbol());
        int countAfter = filledSpaces();

        //determine whether there is a winner
        if (countAfter > countBefore) {
            boolean won = hasWon(whoPlayed.getSymbol());
            if (won) {
                announceWinner(whoPlayed);
            } else if (boardFull()) {
                announceTie();
            } else {
                fireAcceptEvent(playerAfter(whoPlayed));
            }
        } else {
            fireUnAcceptEvent(whoPlayed);//no change.
        }

    }

    private Player playerAfter(Player player) {
        if (player.equals(firstPlayer)) {
            return secondPlayer;
        } else {
            return firstPlayer;
        }
    }

    private void fireUnAcceptEvent(Player player) {
        unAcceptListeners.forEach(unAcceptListener -> {
            UnAcceptEvent unAcceptEvent = new UnAcceptEvent();
            unAcceptEvent.setPlayer(player);
            unAcceptListener.handleUnAcceptEvent(unAcceptEvent);
        });

    }

    private int filledSpaces() {
        return board.filledSpaces();
    }

    public void announceTie() {
        System.out.println("There is no winner");
    }

    public void announceWinner(Player player) {
        System.out.println(
                String.format("Player %s with symbol %s is the winner",
                        player.getName(), player.getSymbol().getValue()));
    }

    public void fireAcceptEvent(Player player) {
        acceptListeners.forEach(acceptListener -> {
            AcceptEvent acceptEvent = new AcceptEvent();
            acceptEvent.setNextPlayer(player);
            acceptListener.handleAcceptEvent(acceptEvent);
        });
    }

    public void addAcceptListeners(AcceptListener acceptListener) {
        acceptListeners.add(acceptListener);
    }

    public void removeAcceptListeners(AcceptListener acceptListener) {
        acceptListeners.remove(acceptListener);
    }

    public void addUnAcceptListeners(UnAcceptListener unAcceptListener) {
        unAcceptListeners.add(unAcceptListener);
    }

    public void start() {
        Location location = firstPlayer.acceptLocation(console);
        firstPlayer.play(location);
    }
}
