package com.bfs.game;

/**
 * Created by lukman on 5/10/16.
 */
public class Location {

    private int row;
    private int column;

    public Location(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int column() {
        return column;
    }

    public int row() {
        return row;
    }

}
