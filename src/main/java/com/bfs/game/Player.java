package com.bfs.game;

import com.bfs.game.event.AcceptEvent;
import com.bfs.game.event.PlayEvent;
import com.bfs.game.event.UnAcceptEvent;
import com.bfs.game.listener.AcceptListener;
import com.bfs.game.listener.PlayListener;
import com.bfs.game.listener.UnAcceptListener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.bfs.game.Symbol.O;

/**
 * The player of the game . The player should be able to identify itself and carry out the
 * following function as specified by the game.
 * Created by lukman on 4/10/16.
 */
public class Player implements AcceptListener, UnAcceptListener {
    private String name;
    private Symbol symbol;
    private List<PlayListener> playListeners = new ArrayList<>(2);


    public Player() {
        this("");
    }

    public Player(String name) {
        this.name = name;
    }

    public void play(Location boardLocation) {
        PlayEvent playEvent = new PlayEvent();
        playEvent.setBoardLocation(boardLocation);
        playEvent.setPlayed(this);
        firePlayEvent(playEvent);
    }

    public void addPlayListener(PlayListener playListener) {
        playListeners.add(playListener);
    }

    public void removePlayListener(PlayListener playListener) {
        playListeners.remove(playListener);
    }

    public String getName() {
        return name;
    }

    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Symbol getSymbol() {
        return this.symbol;
    }

    private void firePlayEvent(PlayEvent playEvent) {
        playListeners.forEach(playListener -> playListener.handlePlayEvent(playEvent));
    }

    @Override
    public void handleAcceptEvent(AcceptEvent acceptEvent) {
        if (acceptEvent.getNextPlayer().equals(this)) {
            Location location = acceptLocation(System.in);
            acceptEvent.getNextPlayer().play(location);
        }

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Player{");
        sb.append("symbol=").append(symbol);
        sb.append(", name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void handleUnAcceptEvent(UnAcceptEvent unAcceptEvent) {
        if (unAcceptEvent.getPlayer().equals(this)) {
            Location location = acceptLocation(System.in);
            unAcceptEvent.getPlayer().play(location);
        }
    }

    public Location acceptLocation(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream);
        System.out.println(name + " Enter a board location :");
        System.out.print("row :");
        int row = scanner.nextInt();
        System.out.print("column :");
        int column = scanner.nextInt();
        return new Location(row, column);
    }

    public void acceptName(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream);
        System.out.print("Enter your name :");
        String name = scanner.next();
        setName(name);
    }

    public void acceptSymbol(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream);
        System.out.print(name + " Enter your symbol ['X' | 'O']:");
        String symbol = scanner.next("^[X|x|o|O]$");
        if (O.getValue().equalsIgnoreCase(symbol)) {
            setSymbol(Symbol.O);
        } else {
            setSymbol(Symbol.X);
        }

    }
}
