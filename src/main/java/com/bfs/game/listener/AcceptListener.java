package com.bfs.game.listener;

import com.bfs.game.event.AcceptEvent;

/**
 * Created by lukman on 5/14/16.
 */
public interface AcceptListener {
    public void handleAcceptEvent(AcceptEvent acceptEvent);
}
