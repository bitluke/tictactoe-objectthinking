package com.bfs.game.listener;


import com.bfs.game.event.PlayEvent;

public interface PlayListener {
    public void handlePlayEvent(PlayEvent playEvent);
}
