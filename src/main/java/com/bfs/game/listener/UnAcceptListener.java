package com.bfs.game.listener;

import com.bfs.game.event.UnAcceptEvent;

/**
 * Created by lukman on 5/14/16.
 */
public interface UnAcceptListener {
    public void handleUnAcceptEvent(UnAcceptEvent unAcceptEvent);
}
