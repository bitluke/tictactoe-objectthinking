package com.bfs.game;

/**
 * This is the main application that is the standalone view of the game.
 */
public class Tictactoe {

    public static void main(String[] args) {
        Engine engine = new Engine();
        engine.initEngine();
        engine.start();
        engine.stop();
    }
}
