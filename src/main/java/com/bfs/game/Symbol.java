package com.bfs.game;

/**
 * Represents the what is handlePlayEvent by player in the game.
 * Created by lukman on 4/10/16.
 */
public enum Symbol {
    X("X"), O("O");

    private String value;

    Symbol(String representation) {
        value = representation;
    }

    public String getValue() {
        return value;
    }
}
