package com.bfs.game;

/**
 * Represent the places where the symbols are placed.It is a 3 * 3 board that is typically a
 * a 3 * 3 array
 * Created by lukman on 4/10/16.
 */
public class Board {
    private static final int ROWS = 3;
    private static final int COLUMNS = 3;
    private Symbol[][] board = new Symbol[ROWS][COLUMNS];
    private static Board INSTANCE = new Board();

    private Board() {
    }

    public void reset() {
        for (int row = 0; row < board.length; row++) {
            for (int column = 0; column < board[row].length; column++) {
                board[row][column] = null;
            }
        }
    }

    public void setLocation(Location location, Symbol symbol) {
        if (!isFull() && isValidLocation(location)) {
            board[location.row()][location.column()] = symbol;
        }
    }

    public boolean isValidLocation(Location location) {
        return isValidRow(location) && isValidColumn(location);
    }

    private boolean isValidRow(Location location) {
        return location.row() >= 0 && location.row() < ROWS;
    }

    private boolean isValidColumn(Location location) {
        return location.column() >= 0 && location.column() < COLUMNS;
    }

    public boolean hasWon(Symbol symbol) {
        return wonVertically(symbol) || wonDiagonally(symbol) || wonHorizontally(symbol);
    }

    private boolean wonHorizontally(Symbol symbol) {
        for (int row = 0; row < ROWS; row++) {
            if (board[row][0] == symbol && board[row][1] == symbol && board[row][2] == symbol) {
                return true;
            }
        }
        return false;
    }

    public boolean isFull() {
        for (int row = 0; row < ROWS; row++) {
            for (int column = 0; column < COLUMNS; column++) {
                if (board[row][column] == null) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean wonVertically(Symbol symbol) {
        for (int column = 0; column < COLUMNS; column++) {
            if (board[0][column] == symbol && board[1][column] == symbol && board[2][column] == symbol) {
                return true;
            }
        }
        return false;
    }

    private boolean wonDiagonally(Symbol symbol) {
        if (board[0][0] == symbol && board[1][1] == symbol && board[2][2] == symbol) {
            return true;
        }
        if (board[2][0] == symbol && board[1][1] == symbol && board[0][2] == symbol) {
            return true;
        }
        return false;
    }

    public static Board getInstance() {
        return INSTANCE;
    }

    public int filledSpaces() {
        int filledSpace = 0;
        for (int row = 0; row < ROWS; row++) {
            for (int column = 0; column < COLUMNS; column++) {
                if (board[row][column] != null) {
                    filledSpace++;
                }
            }
        }
        return filledSpace;
    }

}
